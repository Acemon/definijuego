package com.bluesquare.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bluesquare.game.MyGdxGame;

public class DesktopLauncher {
	/**
	 * Inicio de la aplicacion para pc
	 * @param arg argumantos en la llamada
	 */
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new MyGdxGame(), config);
	}
}
