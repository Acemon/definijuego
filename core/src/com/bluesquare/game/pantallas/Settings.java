package com.bluesquare.game.pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bluesquare.game.MyGdxGame;
import com.bluesquare.game.util.ConexionBBDD;
import com.bluesquare.game.util.ResourceManager;
import com.kotcrab.vis.ui.widget.*;

import java.sql.SQLException;

import static com.bluesquare.game.util.Constantes.MUSICA;
import static com.bluesquare.game.util.Constantes.R_SELECT;

/**
 * Created by Fernando on 17/04/2017.
 */
public class Settings implements Screen {
    private MyGdxGame game;
    private Stage stage;

    private VisCheckBox chkSonido;
    private VisSlider slDificultad;
    private VisLabel lDificultad;
    private VisSelectBox sbPersonaje;
    private VisTextButton btHecho;
    private VisTextField tfPuerto;
    private VisTextField tfUser;
    private VisTextField tfPass;
    private VisTextButton btBBDD;
    private Sound s_sele;

    /**
     * Constructor
     * @param game -->principal
     */
    public Settings(MyGdxGame game) {
        this.game = game;
    }

    /**
     * Parte visual de la aplicacion
     */
    @Override
    public void show() {
        stage = new Stage();

        VisTable table = new VisTable();
        table.setFillParent(true);
        stage.addActor(table);

        s_sele = ResourceManager.obtenerSonido(R_SELECT);

        //Iniciamos todos los componentes
        chkSonido = new VisCheckBox("Musica");

        lDificultad = new VisLabel();
        slDificultad = new VisSlider(1,20,1,false);
        slDificultad.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                lDificultad.setText("Vidas: "+(int) slDificultad.getValue());
            }
        });


        sbPersonaje = new VisSelectBox();
        sbPersonaje.setItems("Melissa", "Rossete");

        btHecho = new VisTextButton("Hecho");
        btHecho.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                guardarPreferencias();
                if (MUSICA)
                    s_sele.play(0.1f);
                game.setScreen(new Main(game));
                dispose();
            }
        });
        VisLabel lConexion = new VisLabel("Conexiones");
        btBBDD = new VisTextButton("Crear BBDD");
        btBBDD.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                try {
                    ConexionBBDD conexion = new ConexionBBDD(btBBDD.getText());
                    conexion.crearBBDD();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });
        tfPuerto = new VisTextField();
        tfUser = new VisTextField();
        tfPass = new VisTextField();
        tfPass.setPasswordMode(true);
        //Metemos lo generado a la tabla
        table.columnDefaults(2);
        table.row();
        table.add();
        table.add(chkSonido).center().pad(4).width(200).height(20);
        table.row();
        table.add(lDificultad);
        table.add(slDificultad).center().pad(0).width(200).height(50);
        table.row();
        table.add(new VisLabel("Jugador"));
        table.add(sbPersonaje).center().pad(4).width(200).height(25);
        table.row();
        table.add(lConexion);
        table.add(btBBDD).center().pad(4).width(200).height(50);
        table.row();
        table.add(new VisLabel("Puerto"));
        table.add(tfPuerto).center().pad(4).width(200).height(50);
        table.row();
        table.add(new VisLabel("Usuario"));
        table.add(tfUser).center().pad(4).width(200).height(50);
        table.row();
        table.add(new VisLabel("Contraseña"));
        table.add(tfPass).center().pad(4).width(200).height(50);
        table.row();
        table.add(btHecho).right().pad(4).width(200).height(50);

        cargaPreferencias();
        lDificultad.setText("Vidas: "+(int) slDificultad.getValue());
        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Carga las preferencias si existen
     */
    private void cargaPreferencias() {
        Preferences preferences = Gdx.app.getPreferences("Definijuego");
        chkSonido.setChecked(preferences.getBoolean("Musica"));
        slDificultad.setValue(preferences.getInteger("Dificultad"));
        sbPersonaje.setSelected(preferences.getString("Personaje"));
        tfPuerto.setText(preferences.getString("Puerto"));
        tfUser.setText(preferences.getString("User"));
        tfPass.setText(preferences.getString("Pass"));
    }

    /**
     * Guarda las preferencias en un props
     */
    private void guardarPreferencias() {
        Preferences preferences = Gdx.app.getPreferences("Definijuego");
        preferences.putBoolean("Musica", chkSonido.isChecked());
        preferences.putInteger("Dificultad", (int) slDificultad.getValue());
        preferences.putString("Personaje", String.valueOf(sbPersonaje.getSelected()));
        preferences.putString("Puerto", tfPuerto.getText());
        preferences.putString("User", tfUser.getText());
        preferences.putString("Pass", tfPass.getText());
        preferences.flush();
    }

    /**
     * resfresco de la aplicacion
     * @param delta Delta time
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
