package com.bluesquare.game.pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.Array;
import com.bluesquare.game.MyGdxGame;
import com.bluesquare.game.npc.*;
import com.bluesquare.game.util.Constantes;
import com.bluesquare.game.util.ResourceManager;

import java.io.File;
import java.util.ArrayList;

import static com.bluesquare.game.util.Constantes.*;

/**
 * Created by Fernando on 17/04/2017.
 */
public class Game implements Screen {
    private Player player;
    private FreeTypeFontGenerator generator;
    private FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    private BitmapFont fuente;
    private MyGdxGame game;
    private OrthographicCamera camera;
    private OrthogonalTiledMapRenderer renderer;
    private Array<Moneda> monedas;
    private Array<Liquido> liquidos;
    private Array<Globo> globos;
    private Array<Bee> abejas;
    private Array<Bug> gusanos;
    private Array<Hedgehog> erizos;
    private ArrayList<String> mapas;
    private ArrayList<String> musica;
    private TiledMap mapa;
    private Batch batch;
    private int contMapas;
    private Music s_mapa;
    private Sound s_coin;
    private Sound s_pow;
    private Sound s_bug;

    /**
     * Controller de la la clase game
     * @param game magia para que esto funcione
     */
    public Game(MyGdxGame game) {
        //Iniciar el jugador a 0, 0 y reasignar al leer el mapa
        if (PERSONAJE.equalsIgnoreCase("Melissa")){
            player = new Player(0, 0, P_HP, P_DMG, walk_p_r, walk_p_l, idle_p);
        }else{
            player = new Player(0, 0, P_HP, P_DMG, walk_q_r, walk_q_l, idle_q);
        }
        //Nombre del archivo de las fuentes
        generator = new FreeTypeFontGenerator(Gdx.files.internal(R_GEN+"font"+ File.separator+"8bit.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        //Tamaño de la fuente in-game
        parameter.size = 15;
        this.game = game;
    }

    /**
     * Inicializa todas las variables que tienen que ver con el funcionamiento del juego
     */
    @Override
    public void show() {
        //Creamos la camara y la posicionamos
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 25*16, 20*16);
        camera.update();
        //Iniciamos los arrays
        monedas = new Array<Moneda>();
        liquidos = new Array<Liquido>();
        abejas = new Array<Bee>();
        gusanos = new Array<Bug>();
        erizos = new Array<Hedgehog>();
        mapas = new ArrayList<String>();
        globos = new Array<Globo>();
        musica = new ArrayList<String>();
        //Añadimos los mapas al array Fin es para que acabe el juego
        mapas.add("Mapa1.tmx");
        mapas.add("Mapa2.tmx");
        mapas.add("Mapa3.tmx");
        mapas.add("Fin");
        contMapas = 0;
        //Añadimos las musicas a los mapas
        musica.add(R_MAP1);
        musica.add(R_MAP2);
        musica.add(R_MAP3);
        cargarMapa(mapas.get(contMapas));
        s_coin = ResourceManager.obtenerSonido(R_COIN);
        s_pow = ResourceManager.obtenerSonido(R_POWER);
        s_bug = ResourceManager.obtenerSonido(R_BUG);
    }

    /**
     * Carga el mapa y lo que contiene como enemigos y dinero
     * @param map Tiled en el que se encuentra
     */
    private void cargarMapa(String map) {
        if (map.equals("Fin")){
            game.setScreen(new GameOver(game, player.puntuacion));
            return;
        }
        s_mapa = ResourceManager.obtenerMusica(musica.get(contMapas));
        s_mapa.setLooping(true);
        s_mapa.setVolume(0.2f);
        contMapas ++;
        mapa = new TmxMapLoader().load(R_GEN+"Mapas"+File.separator+map);
        renderer = new OrthogonalTiledMapRenderer(mapa);
        batch = renderer.getBatch();
        fuente = generator.generateFont(parameter);
        renderer.setView(camera);
        if (MUSICA)
            s_mapa.play();
        cargarEnemigos();
        cargarDineroyAgua();
        cargarPowerUps();
    }

    /**
     * Carga los poerups que recoge el jugador
     */
    private void cargarPowerUps() {
        MapLayer ml = mapa.getLayers().get("PowerUp");
        if (ml!= null){
            MapObjects mapObjects = ml.getObjects();
            for (MapObject mo : mapObjects){
                String tipo = (String) mo.getProperties().get("tipo");
                TiledMapTileMapObject tile = (TiledMapTileMapObject) mo;
                if (tipo.equalsIgnoreCase("Globo")){
                    Globo globo = new Globo(tile.getX(), tile.getY());
                    globos.add(globo);
                }
            }
        }
    }

    /**
     * Carga las monedas que el jugador debe recoger y los añade al array
     */
    private void cargarDineroyAgua() {
        MapLayer ml = mapa.getLayers().get("Dinero");
        MapObjects mapObjects = ml.getObjects();
        //Liquidos
        ml = mapa.getLayers().get("Liquido");
        if (ml!=null){
            for (MapObject obj : ml.getObjects()){
                mapObjects.add(obj);
            }
        }
        for(MapObject map : mapObjects){
            String tipo = (String) map.getProperties().get("tipo");
            if (tipo.equalsIgnoreCase("Plata") || tipo.equalsIgnoreCase("Oro")){
                int puntuacion = Integer.parseInt((String) map.getProperties().get("puntuacion"));
                TiledMapTileMapObject tile = (TiledMapTileMapObject) map;
                Moneda moneda = new Moneda(tile.getX(), tile.getY(), puntuacion);
                monedas.add(moneda);
            }
            if (tipo.equalsIgnoreCase("Agua") || tipo.equalsIgnoreCase("Lava")){
                String liquidillo = (String) map.getProperties().get("tipo");
                TiledMapTileMapObject tile = (TiledMapTileMapObject) map;
                Liquido liquido = new Liquido(tile.getX(), tile.getY(), liquidillo);
                liquidos.add(liquido);
            }
        }
    }

    /**
     * Carga los enemigos (Y el jugador) de cada mapa y los añade al array
     */
    private void cargarEnemigos() {
        MapLayer ml = mapa.getLayers().get("Npc");
        if (ml!= null){
            MapObjects mapObjects = ml.getObjects();
            for(MapObject mo : mapObjects){
                String tipo = (String) mo.getProperties().get("tipo");
                int dmg = Integer.parseInt((String) mo.getProperties().get("dmg"));
                int hp = Integer.parseInt((String) mo.getProperties().get("hp"));
                TiledMapTileMapObject tile = (TiledMapTileMapObject) mo;
                //Añadimos los npcs a cada uno de los vectores
                if (tipo.equalsIgnoreCase("Abeja")){
                    Bee abeja = new Bee(tile.getX(), tile.getY(), hp, dmg);
                    abejas.add(abeja);
                }
                if (tipo.equalsIgnoreCase("Gusano")){
                    Bug gusano = new Bug(tile.getX(), tile.getY(), hp, dmg, player);
                    gusanos.add(gusano);
                }
                if (tipo.equalsIgnoreCase("Erizo")){
                    Hedgehog erizo = new Hedgehog(tile.getX(), tile.getY(), hp, dmg, player);
                    erizos.add(erizo);
                }
                if (tipo.equalsIgnoreCase("Jugador")){
                    player.posicion.x = tile.getX();
                    player.posicion.y = tile.getY();
                }
            }
        }
    }

    /**
     * Clase que permite el refresco de todas las acciones que ocurren en el juego
     * @param dt tiempo
     */
    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //Carga tantas capas como marques en el vector, en este caso 3 capas
        renderer.render(new int[]{0,1,2});
        batch.begin();
        //Hacemos render a todos los personajes
        for (Bee abeja : abejas){
            abeja.render(batch);
        }
        for (Bug gusano : gusanos){
            gusano.render(batch);
        }
        for (Hedgehog erizo : erizos){
            erizo.render(batch);
        }
        for (Moneda moneda : monedas){
            moneda.render(batch);
        }
        for (Liquido liquido : liquidos){
            liquido.render(batch);
        }
        for (Globo globo : globos){
            globo.render(batch);
        }
        player.render(batch);
        fuente.draw(batch, "Puntuacion: "+player.puntuacion, camera.position.x-(25*16/2), camera.position.y+(19*16/2));
        fuente.draw(batch, "Vidas: "+player.hp, camera.position.x-(25*16/2), camera.position.y+(17*16/2));
        batch.end();
        
        //Hacemos Update, colisiones y movimiento de los personajes
        player.update(dt);
        player.move(Constantes.MOVIMINETO, dt);
        player.colision(mapa);
        //Muerto
        if (player.hp <= 0){
            limpiar();
            game.setScreen(new GameOver(game, player.puntuacion));
            dispose();
        }
        for (Bee abeja : abejas){
            abeja.update(dt);
            abeja.move(Constantes.MOVIMINETO, dt);
            abeja.colision(mapa);
            if (abeja.hp <= 0){
                abejas.removeValue(abeja, true);
            }
            if (player.hitbox.overlaps(abeja.hitbox)){
                if (!player.invulnerable){
                    player.invulnerabilidad();
                    player.hp-=abeja.dmg;
                }
            }
        }
        for (Hedgehog erizo : erizos){
            erizo.update(dt);
            erizo.move(Constantes.MOVIMINETO, dt);
            erizo.colision(mapa);
            if (erizo.hp <= 0){
                erizos.removeValue(erizo, true);
            }
            if (player.hitbox.overlaps(erizo.hitbox)){
                if (!player.invulnerable){
                    player.hp-=erizo.dmg;
                    player.invulnerabilidad();
                }
            }
        }
        for (Bug gusano : gusanos){
            gusano.update(dt);
            gusano.move(Constantes.MOVIMINETO, dt);
            gusano.colision(mapa);
            if (gusano.hp <=0){
                gusanos.removeValue(gusano, true);
            }
            if (player.hitbox.overlaps(gusano.hitbox)){
                if (player.posicion.y > gusano.posicion.y && player.velocidad.y<=0){
                    if (MUSICA)
                        s_bug.play(0.01f);
                    gusano.hp-=player.dmg;
                }else if (!player.invulnerable){
                    player.hp-=gusano.dmg;
                    player.invulnerabilidad();
                }
            }
        }
        for (Moneda moneda : monedas){
            moneda.update(dt);
            if (player.hitbox.overlaps(moneda.rect)){
                if (MUSICA)
                    s_coin.play(0.05f);
                player.puntuacion+=moneda.puntuacion;
                monedas.removeValue(moneda, true);
            }
        }
        for (Liquido liquido : liquidos){
            liquido.update(dt);
        }
        for (Globo globo : globos){
            if (player.hitbox.overlaps(globo.rect)){
                if (MUSICA)
                    s_pow.play(0.03f);
                player.modoGlobo(true);
                globo.rect.x = player.posicion.x;
                globo.rect.y = player.posicion.y;
                if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
                    globos.removeValue(globo, true);
                    player.modoGlobo(false);
                }
            }
        }
        fijarCamara();
        pasarNivel();
    }

    /**
     * Comprueba si puede pasar de nivil y en caso afirmativo, pasa
     */
    private void pasarNivel() {
        if (player.pasarnivel){
            limpiar();
            cargarMapa(mapas.get(contMapas));
            player.pasarnivel=false;
        }
    }

    /**
     * Limpia los vectores de enemigos y dinero para cargar un mapa nuevo
     */
    private void limpiar() {
        gusanos.clear();
        erizos.clear();
        abejas.clear();
        monedas.clear();
        liquidos.clear();
        globos.clear();
        if (s_mapa!=null){
            s_mapa.stop();
        }
    }

    /**
     * Posiciona la camara centrada en el personaje, y si esta en un limite del mapa, se ajusta
     * para que no se vea el margen
     */
    private void fijarCamara() {
        if (player.posicion.x < 25*16/2){
            camera.position.x = 25*16/2;
        }else if (player.posicion.x > ANCHOTILED-25*16/2){
            camera.position.x = ANCHOTILED-(25*16/2);
        }else{
            camera.position.x = player.posicion.x;
        }
        if (player.posicion.y < 20*16/2){
            camera.position.y = 20*16/2;
        }else if (player.posicion.y > ALTOILED-20*16/2){
            camera.position.y = ALTOILED-(20*16/2);
        }else{
            camera.position.y = player.posicion.y;
        }
        camera.update();
        renderer.setView(camera);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void
    dispose() {

    }
}
