package com.bluesquare.game.pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bluesquare.game.MyGdxGame;
import com.bluesquare.game.util.Constantes;
import com.bluesquare.game.util.ResourceManager;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

import static com.bluesquare.game.util.Constantes.*;


/**
 * Created by Fernando on 17/04/2017.
 */
public class Main implements Screen {
    private MyGdxGame game;
    Stage stage;
    private Music s_Main;
    private Sound s_select;

    /**
     * Constructor
     * @param game -->principal
     */
    public Main(MyGdxGame game) {
        this.game = game;
    }

    /**
     * Parte visual de la aplicacion
     */
    @Override
    public void show() {
        //Cargamos los componentes de VisUI si no estan cargados
        if (!VisUI.isLoaded())
            VisUI.load();
        stage = new Stage();
        //Cargar recursos del ResourceManager
        ResourceManager.cargarRecursos();
        cargarPreferencias();
        s_select = ResourceManager.obtenerSonido(R_SELECT);
        s_Main = ResourceManager.obtenerMusica(R_MAIN);
        if (MUSICA) {
            s_Main.setLooping(true);
            s_Main.setVolume(0.3f);
            if (!s_Main.isPlaying())
                s_Main.play();
        }
        //Creamos una tabla, la ajustamos a los bordes y la añadimos al escenario
        VisTable table = new VisTable();
        table.setFillParent(true);
        stage.addActor(table);

        //Creamos los diferentes botones
        VisTextButton btJugar = new VisTextButton("Jugar");
        btJugar.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                //Lanza la pantalla juego
                if (s_Main!=null)
                    s_Main.stop();
                if (MUSICA)
                    s_select.play(0.1f);
                game.setScreen(new Game(game));
                dispose();
            }
        });

        VisTextButton btConfig = new VisTextButton("Configuracion");
        btConfig.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (s_Main!=null)
                    s_Main.stop();
                if (MUSICA)
                    s_select.play(0.1f);
                game.setScreen(new Settings(game));
                dispose();
            }
        });

        VisTextButton btSalir = new VisTextButton("Salir");
        btSalir.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.exit(0);
            }
        });

        //Añadimos los botones a la tabla
        table.row();
        table.add(btJugar).center().pad(5).width(200).height(50);
        table.row();
        table.add(btConfig).center().pad(5).width(200).height(50);
        table.row();
        table.add(btSalir).center().pad(5).width(200).height(50);

        Gdx.input.setInputProcessor(stage);
    }

    private void cargarPreferencias() {
        Preferences preferences = Gdx.app.getPreferences("Definijuego");
        P_HP = preferences.getInteger("Dificultad",5);
        MUSICA = preferences.getBoolean("Musica", true);
        PERSONAJE = preferences.getString("Personaje", "Melissa");
        PUERTO = preferences.getString("Puerto", "3306");
        USUARIO = preferences.getString("User", "root");
        PSWD = preferences.getString("Pass", "");
    }

    /**
     * resfresco de la aplicacion
     * @param delta Delta time
     */
    @Override
    public void render(float delta) {
        //TODO Poner un fondo
        Gdx.gl.glClearColor(1, 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
