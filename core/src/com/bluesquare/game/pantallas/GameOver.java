package com.bluesquare.game.pantallas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bluesquare.game.MyGdxGame;
import com.bluesquare.game.util.ConexionBBDD;
import com.bluesquare.game.util.Constantes;
import com.bluesquare.game.util.ResourceManager;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.bluesquare.game.util.Constantes;

import java.sql.*;

import static com.bluesquare.game.util.Constantes.MUSICA;
import static com.bluesquare.game.util.Constantes.R_SELECT;

/**
 * Created by Fernando on 17/04/2017.
 */
public class GameOver implements Screen{
    private MyGdxGame game;
    private Stage stage;
    private int puntuacion;
    private Sound s_sele;

    public GameOver(MyGdxGame game, int puntuacion) {
        this.game = game;
        this.puntuacion = puntuacion;
    }

    /**
     * Inicia las variables y establece la tabla
     */
    @Override
    public void show() {
        stage = new Stage();
        VisTable tabla = new VisTable();
        tabla.setFillParent(true);
        stage.addActor(tabla);
        s_sele = ResourceManager.obtenerSonido(R_SELECT);

        VisLabel lPuntuacion = new VisLabel("Has conseguido "+puntuacion+" puntos");
        //Fixme parte de bbdd
        VisLabel lBasededatos = new VisLabel();
        try {
            ConexionBBDD bbdd = new ConexionBBDD();
            bbdd.insertarPuntuacion(puntuacion);
            lBasededatos.setText("Puntuacion Maxima: "+bbdd.buscarPosicion());
        } catch (ClassNotFoundException e) {
            lBasededatos.setText("Error al iniciar el driver");
            e.printStackTrace();
        } catch (SQLException e) {
            lBasededatos.setText("Error al conectar la BBDD");
            e.printStackTrace();
        }

        VisTextButton btReinicio = new VisTextButton("Reiniciar");
        btReinicio.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (MUSICA)
                    s_sele.play(0.1f);
                game.setScreen(new Game(game));
                dispose();
            }
        });
        VisTextButton btMenu = new VisTextButton("Menu Principal");
        btMenu.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (MUSICA)
                    s_sele.play(0.1f);
                game.setScreen(new Main(game));
                dispose();
            }
        });

        tabla.row();
        tabla.add(lPuntuacion).center().pad(5).width(200).height(50);
        tabla.row();
        tabla.add(lBasededatos).center().pad(5).width(200).height(50);
        tabla.row();
        tabla.add(btReinicio).center().pad(5).width(200).height(50);
        tabla.row();
        tabla.add(btMenu).center().pad(5).width(200).height(50);

        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Refresca los datos que aparecen por pantalla
     * @param delta
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
