package com.bluesquare.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bluesquare.game.pantallas.Main;

public class MyGdxGame extends Game {
	SpriteBatch batch;
	
	@Override
	/**
	 * Inicia la aplicacion
	 */
	public void create () {
		batch = new SpriteBatch();
		setScreen(new Main(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
