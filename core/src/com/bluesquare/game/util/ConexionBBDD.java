package com.bluesquare.game.util;

import java.sql.*;

import static com.bluesquare.game.util.Constantes.*;

/**
 * Created by Fernando on 28/05/2017.
 */
public class ConexionBBDD {
    public Connection conexion;

    /**
     * Conexion a base de datos
     * @throws ClassNotFoundException no encuentra la clase
     * @throws SQLException error de sintaxis sql
     */
    public ConexionBBDD() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        conexion = DriverManager.getConnection("jdbc:mysql://"+IP+":"+PUERTO+"/"+BBDD,USUARIO,PSWD);
    }

    public ConexionBBDD(CharSequence text) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        conexion = DriverManager.getConnection("jdbc:mysql://"+IP+":"+PUERTO,USUARIO,PSWD);
    }

    /**
     * Inserta la puntuacion en la bbdd
     * @param puntos cantidad
     * @throws ClassNotFoundException no encuentra la clase
     * @throws SQLException error de sintaxis sql
     */
    public void insertarPuntuacion(int puntos) throws ClassNotFoundException, SQLException {
        String sentencia = "INSERT INTO puntuaciones (puntuacion) VALUES (?)";
        PreparedStatement ps = conexion.prepareStatement(sentencia);
        ps.setInt(1, puntos);
        ps.execute();
        ps.close();
    }

    /**
     * Busca la puntuacion maxima
     * @return devuelve la puntuacion mayor
     * @throws SQLException error de sintaxis sql
     */
    public Integer buscarPosicion() throws SQLException {
        Integer posicion;
        String sentencia = "SELECT puntuacion FROM puntuaciones ORDER BY puntuacion DESC LIMIT 1";
        PreparedStatement ps = conexion.prepareStatement(sentencia);
        ResultSet rs = ps.executeQuery();
        rs.next();
        posicion = rs.getInt(1);
        return posicion;
    }

    public void crearBBDD() throws SQLException {
        String crear = "create database if not EXISTS videojuego";
        String seleecionar = "use videojuego";
        String tabla = "create table if not exists puntuaciones(puntuacion int)";
        Statement ps = conexion.createStatement();
        ps.execute(crear);
        ps.execute(seleecionar);
        ps.execute(tabla);
        ps.close();
    }
}
