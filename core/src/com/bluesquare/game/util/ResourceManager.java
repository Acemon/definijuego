package com.bluesquare.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import static com.bluesquare.game.util.Constantes.*;

/**
 * Created by DTUser on 21/04/2017.
 */
public class ResourceManager {
    private static AssetManager assetManager = new AssetManager();

    /**
     * Precarga los recursos para poder usarlos
     */
    public static void cargarRecursos(){
        if (!assetManager.isLoaded(R_MAPA))
            assetManager.load(R_MAPA, TextureAtlas.class);

        if (!assetManager.isLoaded(R_SALTO))
            assetManager.load(R_SALTO, Sound.class);

        if (!assetManager.isLoaded(R_BUG))
            assetManager.load(R_BUG, Sound.class);

        if (!assetManager.isLoaded(R_COIN))
            assetManager.load(R_COIN, Sound.class);

        if (!assetManager.isLoaded(R_POWER))
            assetManager.load(R_POWER, Sound.class);

        if (!assetManager.isLoaded(R_SELECT))
            assetManager.load(R_SELECT, Sound.class);

        if (!assetManager.isLoaded(R_DAMAGED))
            assetManager.load(R_DAMAGED, Sound.class);

        if (!assetManager.isLoaded(R_MAIN))
            assetManager.load(R_MAIN, Music.class);

        if (!assetManager.isLoaded(R_MAP1))
            assetManager.load(R_MAP1, Music.class);

        if (!assetManager.isLoaded(R_MAP2))
            assetManager.load(R_MAP2, Music.class);

        if (!assetManager.isLoaded(R_MAP3))
            assetManager.load(R_MAP3, Music.class);

        assetManager.finishLoading();
    }

    /**
     * Obtiene un frame
     * @return frame
     * @param frame nombre del frame
     */
    public static TextureRegion obtenerFrame(String frame){
        return assetManager.get(R_MAPA, TextureAtlas.class).findRegion(frame);
    }

    /**
     * Obtiene una animacion
     * @param anim nombre de la animcion
     * @return animacion
     */
    public static Array<TextureAtlas.AtlasRegion> obtenerAnimacion(String anim) {
        return assetManager.get(R_MAPA, TextureAtlas.class).findRegions(anim);
    }

    /**
     * Obtiene un sonido
     * @param sonido nombre del sonido
     * @return archivo de sonido
     */
    public static Sound obtenerSonido(String sonido){
        return assetManager.get(sonido, Sound.class);
    }

    /**
     * Obtiene musica
     * @param musica nombre de la musca
     * @return archivo de musica
     */
    public static Music obtenerMusica (String musica){
        return assetManager.get(musica, Music.class);
    }
}
