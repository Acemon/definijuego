package com.bluesquare.game.util;

import java.sql.*;

/**
 * Created by DTUser on 21/04/2017.
 */
public class Constantes {
    public static float GRAVEDAD = 6f;
    public static int P_HP;
    public static final int P_DMG = 2;
    public static final int MOVIMINETO = 110;
    public static String COLISION = "Colision";
    public static float ANCHOTILED = 150*16;
    public static float ALTOILED = 60*16;
    public static float SALTO = 8;
    public static Boolean MUSICA;
    //Sonidos
    public static final String R_GEN = "core/assets/";
    public static final String R_SALTO = R_GEN+"Musica/Jump.wav";
    public static final String R_BUG = R_GEN+"Musica/Bug.wav";
    public static final String R_COIN = R_GEN+"Musica/Coin.wav";
    public static final String R_POWER = R_GEN+"Musica/Pow.wav";
    public static final String R_SELECT = R_GEN+"Musica/Select.wav";
    public static final String R_DAMAGED = R_GEN+"Musica/Damaged.ogg";
    //Atlas
    public static final String R_MAPA = R_GEN+"atlas.pack";
    //Musicas
    public static final String R_MAIN = R_GEN+"Musica/Main.mp3";
    public static final String R_MAP1 = R_GEN+"Musica/Map1.mp3";
    public static final String R_MAP2 = R_GEN+"Musica/Map2.mp3";
    public static final String R_MAP3 = R_GEN+"Musica/Map3.ogg";
    //Personaje
    public static String PERSONAJE;
    public static final String walk_p_l = "walk_p_l";
    public static final String walk_p_r = "walk_p_r";
    public static final String idle_p = "idle_p";
    public static final String walk_q_l = "walk_q_l";
    public static final String walk_q_r = "walk_q_r";
    public static final String idle_q = "idle_q";
    //Base de datos
    public static final String IP = "localhost";
    public static String USUARIO;
    public static String PSWD;
    public static String PUERTO;
    public static final String BBDD = "videojuego";


}
