package com.bluesquare.game.npc;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.bluesquare.game.util.ResourceManager;

/**
 * Created by Fernando on 14/05/2017.
 */
public class Globo {
    private TextureRegion frame;
    public Rectangle rect;

    /**
     * Constructor del globo
     * @param x posicion x
     * @param y posicion y
     */
    public Globo(float x, float y){
        frame = new TextureRegion(ResourceManager.obtenerFrame("globo"));
        rect = new Rectangle(x, y, frame.getRegionWidth(), frame.getRegionHeight());
    }

    /**
     * Dibuja el globo
     * @param batch Magia para dibujar
     */
    public void render(Batch batch){
        batch.draw(frame, rect.x, rect.y);
    }
}
