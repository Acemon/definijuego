package com.bluesquare.game.npc;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.bluesquare.game.util.ResourceManager;

import static com.bluesquare.game.util.Constantes.*;

public class Character{
    public TextureRegion frameActual;
    protected Animation animacionDer;
    protected Animation animacionIzq;
    protected Animation animacionIdle;
    public Vector2 posicion;
    public Vector2 velocidad;
    public Rectangle hitbox;
    public Estado estado;
    public float tiempo;
    public int hp;
    public int dmg;
    public boolean invulnerable;
    protected Boolean nullGravedad;
    protected boolean flotar;
    protected boolean saltable;
    private Sound s_damaged;

    /**
     * Constructor de Character
     * @param x -->Posicion en el eje X
     * @param y -->Posicion el el eje Y
     * @param aniDer -->Animacion Derecha
     * @param aniIzq -->Animacion Izquierda
     * @param aniIdle -->Animacion IDLE
     * @param hp -->Salud
     * @param dmg -->Daño que realiza
     */
    public Character(float x, float y, String aniDer, String aniIzq, String aniIdle, int hp, int dmg){
        this.posicion = new Vector2(x,y);
        this.velocidad = new Vector2(0,0);
        this.hp = hp;
        this.dmg = dmg;
        this.tiempo = 0;
        this.estado = Estado.QUIETO;
        this.nullGravedad=false;
        this.flotar = false;
        this.saltable = false;
        s_damaged = ResourceManager.obtenerSonido(R_DAMAGED);

        this.animacionDer = new Animation(0.1f, ResourceManager.obtenerAnimacion(aniDer));
        this.animacionIzq = new Animation(0.1f, ResourceManager.obtenerAnimacion(aniIzq));
        this.animacionIdle = new Animation(0.5f, ResourceManager.obtenerAnimacion(aniIdle));

        frameActual = (TextureRegion) animacionIdle.getKeyFrame(0);
        hitbox = new Rectangle(posicion.x, posicion.y, frameActual.getRegionWidth(), frameActual.getRegionHeight());
    }

    /**
     * Gesiona las colisiones del npc con las cosas que tiene el mapa
     * @param mapa -->Tiled
     */
    public void colision(TiledMap mapa){
        //Nombre de capa
        saltable = false;
        MapLayer colision = mapa.getLayers().get(COLISION);
        MapObjects mapObject = colision.getObjects();
        for (MapObject map : mapObject){
            Rectangle rect =((RectangleMapObject)map).getRectangle();
            if (rect.overlaps(hitbox)){
                //Nombre del atributo
                String hit = (String) map.getProperties().get("tipo");
                if (hit.equalsIgnoreCase("Lava")){
                    hp=0;
                }
                if (hit.equalsIgnoreCase("Muerte")){
                    hp=0;
                }
                if (hit.equalsIgnoreCase("Pincho")){
                    hp-=2;
                }
                if (hit.equalsIgnoreCase("Salto")){
                    velocidad.y = SALTO*1.5f;
                }
                if (hit.equalsIgnoreCase("Suelo")){
                    posicion.y = rect.getY();
                    saltable=true;
                }else{
                    if (hit.equalsIgnoreCase("Derecha")){
                        posicion.x = rect.getX();
                    }
                    if (hit.equalsIgnoreCase("Izquierda")){
                        posicion.x = rect.getX()-frameActual.getRegionWidth();
                    }
                    if (hit.equalsIgnoreCase("Techo")){
                        posicion.y = rect.getY()-frameActual.getRegionHeight();
                        velocidad.y = 0;
                    }
                }
            }
        }
    }

    /**
     * Gestiona las acciones basicas cada frame del juego
     * @param dt -->Delta time
     */
    public void update(float dt){
        //Gestion de animaciones
        tiempo += dt;
        switch (estado){
            case QUIETO:
                frameActual = (TextureRegion) animacionIdle.getKeyFrame(tiempo, true);
                break;
            case DERECHA:
                frameActual = (TextureRegion) animacionDer.getKeyFrame(tiempo, true);
                break;
            case IZQUIERDA:
                frameActual = (TextureRegion) animacionIzq.getKeyFrame(tiempo, true);
                break;
        }
        //Gestion de Gravedad
        if (!nullGravedad && !flotar){
            velocidad.y -=GRAVEDAD*dt;
            if (velocidad.y < GRAVEDAD){
                velocidad.y = -GRAVEDAD;
            }
        }else if (flotar){
            velocidad.y -=GRAVEDAD*dt/15;
            if (velocidad.y < GRAVEDAD/15){
                velocidad.y = -GRAVEDAD/15;
            }
        }
        posicion.add(velocidad);
        //Ajusta la hitbox al personaje
        hitbox.setPosition(posicion);
    }

    /**
     * Metodo para dibujar el character
     * @param batch magia que dibuja
     */
    public void render(Batch batch){
        batch.draw(frameActual, posicion.x, posicion.y);
    }

    /**
     * Gestiona el tiemp de invulnerabilidad para que no reciba mas de 1 hit en X segundos
     */
    public void invulnerabilidad(){
        if(MUSICA)
            s_damaged.play(0.1f);
        invulnerable = true;
        Timer.schedule(new Timer.Task(){
            @Override
            public void run() {
                invulnerable=false;
            }
        }, 2);
    }

    /**
     * Gestiona los limites del Tiled y recoloca al npc dentro del mapa
     * @param mov -->movimiento
     * @param dt -->Delta time
     */
    public void move(int mov, float dt) {
        //Ajusta la Horizontal
        if (posicion.x+frameActual.getRegionWidth() >= ANCHOTILED){
            posicion.x = ANCHOTILED-frameActual.getRegionWidth();
        }else if (posicion.x <= 0){
            posicion.x = 0;
        }
        //Ajusta la Vertical
        if (posicion.y+frameActual.getRegionHeight() >= ALTOILED){
            posicion.y = ALTOILED -frameActual.getRegionHeight();
        }else if (posicion.y <= 0){
            posicion.y = 0;
        }
    }

    /**
     * Enumeracion que indica la posicion del NPC
     */
    public enum Estado{
        DERECHA, IZQUIERDA, QUIETO;
    }
}