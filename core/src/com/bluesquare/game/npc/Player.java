package com.bluesquare.game.npc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.bluesquare.game.util.ResourceManager;

import static com.bluesquare.game.util.Constantes.*;

public class Player extends Character {
    public boolean pasarnivel;
    public int puntuacion;
    private Sound s_salto;
    /**
     * Constructor de Player
     * @param x -->super
     * @param y -->super
     * @param hp -->super
     * @param dmg -->super
     */
    public Player(int x, int y, int hp, int dmg, String der, String izq, String idl) {
        super(x, y, izq, der, idl, hp, dmg);
        puntuacion = 0;
        invulnerable = false;
        pasarnivel = false;
        s_salto = ResourceManager.obtenerSonido(R_SALTO);
    }

    /**
     * Input de teclado para el jugador
     * @param mov -->movimiento
     * @param dt -->Delta time
     */
    @Override
    public void move(int mov, float dt) {
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            velocidad.x = mov*dt;
            estado = Estado.DERECHA;
        }else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            velocidad.x = -mov*dt;
            estado = Estado.IZQUIERDA;
        }else{
            velocidad.x = 0;
            estado = Estado.QUIETO;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && saltable){
            velocidad.y = SALTO;
            if (MUSICA)
                s_salto.play(0.05f);
        }
        super.move(mov, dt);
    }

    /**
     * Modifica el boleano flotarde character
     * @param a
     */
    public void modoGlobo(boolean a){
        flotar = a;
    }

    /**
     * Colision que solo el jugador puede actuar con ellas
     * @param mapa -->Tiled
     */
    @Override
    public void colision(TiledMap mapa) {
        super.colision(mapa);
        //Nombre de capa
        MapLayer colision = mapa.getLayers().get(COLISION);
        MapObjects mapObject = colision.getObjects();
        for (MapObject map : mapObject){
            Rectangle rect =((RectangleMapObject)map).getRectangle();
            if (rect.overlaps(hitbox)) {
                //Nombre del atributo
                String hit = (String) map.getProperties().get("tipo");
                if (hit.equalsIgnoreCase("Fin")){
                    pasarnivel = true;
                }
                if (hit.equalsIgnoreCase("Escalera")){
                    nullGravedad=true;
                    saltable = true;
                }else if (hit.equalsIgnoreCase("Gravedad")){
                    nullGravedad=false;
                }
                if (hit.equalsIgnoreCase("Agua")){
                    saltable = true;
                    nullGravedad=true;
                    if (velocidad.y > 2){
                        velocidad.y-=0.2;
                    }else if (velocidad.y < 2){
                        velocidad.y+=0.2;
                    }
                }
            }
        }
    }
}
