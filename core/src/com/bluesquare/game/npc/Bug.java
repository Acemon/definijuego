package com.bluesquare.game.npc;

/**
 * Created by DTUser on 21/04/2017.
 */
public class Bug extends Character {
    private Player jugador;

    /**
     * Constructor de Bug
     * @param x -->super
     * @param y -->super
     * @param hp -->super
     * @param dmg -->super
     * @param jugador -->Jugador actual
     */
    public Bug(float x, float y, int hp, int dmg, Player jugador) {
        super(x, y, "walk_b_r", "walk_b_l", "idle_b", hp, dmg);
        invulnerable = false;
        this.jugador = jugador;
    }

    /**
     * IA del Bug
     * @param mov -->movimiento
     * @param dt -->Delta time
     */
    @Override
    public void move(int mov, float dt) {
        int movi = mov/2;
        if (jugador.posicion.x > this.posicion.x){
            velocidad.x = movi*dt;
            estado = Estado.DERECHA;
        }else if (jugador.posicion.x < this.posicion.x){
            velocidad.x = -movi*dt;
            estado = Estado.IZQUIERDA;
        }else {
            estado= Estado.QUIETO;
        }
        super.move(movi, dt);
    }
}
