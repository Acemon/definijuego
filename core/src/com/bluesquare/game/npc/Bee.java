package com.bluesquare.game.npc;

import com.badlogic.gdx.utils.Timer;

/**
 * Created by Fernando on 25/04/2017.
 */
public class Bee extends Character{
    boolean movimiento;
    boolean moverse;
    /**
     * Constructor de Bee
     * @param x       -->super
     * @param y       -->super
     * @param hp      -->super
     * @param dmg     -->super
     */
    public Bee(float x, float y, int hp, int dmg) {
        super(x, y, "walk_be_r", "walk_be_l", "walk_be_r", hp, dmg);
        movimiento = true;
        moverse= true;
        nullGravedad = true;
    }

    /**
     * Metodo para que la abeja se mueva
     * @param mov -->movimiento
     * @param dt -->Delta time
     */
    @Override
    public void move(int mov, float dt) {
        if (moverse){
            Timer.schedule(new Timer.Task(){
                @Override
                public void run() {
                    if (movimiento)
                        movimiento=false;
                    else
                        movimiento=true;
                    moverse=true;
                }
            }, 2);
            moverse=false;
        }
        if (movimiento){
            velocidad.x = mov*dt;
            estado = Estado.DERECHA;
        }
        else{
            velocidad.x = -mov*dt;
            estado = Estado.IZQUIERDA;
        }
    }
}