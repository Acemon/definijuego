package com.bluesquare.game.npc;

import com.badlogic.gdx.utils.Timer;

/**
 * Created by Fernando on 25/04/2017.
 */
public class Hedgehog extends Character {
    private Player player;
    private boolean atacar;
    private int atacando;
    /**
     * Constructor de Hedgehog
     * @param x -->super
     * @param y -->super
     * @param hp -->super
     * @param dmg -->super
     */
    public Hedgehog(float x, float y, int hp, int dmg, Player player) {
        super(x, y, "walk_h_r", "walk_h_l", "idle_h", hp, dmg);
        this.player = player;
        atacar = true;
        atacando = 0;
    }

    /**
     * Metodo para mover al erizo
     * @param mov -->movimiento
     * @param dt -->Delta time
     */
    @Override
    public void move(int mov, float dt) {
        super.move(mov, dt);
        if (player.posicion.x>this.posicion.x-150 && player.posicion.x<this.posicion.x){
            if (atacar){
                Timer.schedule(new Timer.Task(){
                    @Override
                    public void run() {
                        atacando = -2;
                        atacar = true;
                    }
                }, 1);
                atacar = false;
            }
            estado = Estado.IZQUIERDA;
        }else if (player.posicion.x<this.posicion.x+150 && player.posicion.x> this.posicion.x){
            if (atacar){
                Timer.schedule(new Timer.Task(){
                    @Override
                    public void run() {
                        atacando = +2;
                        atacar = true;
                    }
                }, 1);
                atacar = false;
            }
            estado = Estado.DERECHA;
        }else{
            atacando = 0;
        }
        posicion.x += atacando;
    }
}
