package com.bluesquare.game.npc;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.bluesquare.game.util.ResourceManager;

/**
 * Created by Fernando on 25/04/2017.
 */
public class Moneda {
    private Animation image;
    private TextureRegion frame;
    public Rectangle rect;
    public int puntuacion;
    private float tiempo;

    /**
     * Constructor de moneda
     * @param x posicion x
     * @param y posicion y
     * @param puntuacion puntos que otorga
     */
    public Moneda(float x, float y, int puntuacion) {
        tiempo=0;
        if (puntuacion == 50) {
            image = new Animation(0.1f, ResourceManager.obtenerAnimacion("coin_s"));
        }
        if (puntuacion == 100) {
            image = new Animation(0.1f, ResourceManager.obtenerAnimacion("coin_g"));
        }
        frame = (TextureRegion) image.getKeyFrame(0);
        rect = new Rectangle(x, y, frame.getRegionWidth(), frame.getRegionHeight());
        this.puntuacion = puntuacion;
    }

    /**
     * Selecciona la siguiente textura
     * @param dt delta time
     */
    public void update(float dt){
        tiempo += dt;
        frame = (TextureRegion) image.getKeyFrame(tiempo, true);
    }

    /**
     * Dibuja la moneda
     * @param batch magia que dibuja
     */
    public void render(Batch batch){
        batch.draw(frame, rect.x, rect.y);
    }
}
