package com.bluesquare.game.npc;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.bluesquare.game.util.ResourceManager;

/**
 * Created by DAM on 11/05/2017.
 */
public class Liquido {
    private Animation image;
    private TextureRegion frame;
    private Rectangle rect;
    private float tiempo;

    /**
     * Constructor del liquido
     * @param x
     * @param y
     * @param tipo
     */
    public Liquido(float x, float y, String tipo) {
        tiempo = 0;
        if (tipo.equalsIgnoreCase("Agua")){
            image = new Animation(0.2f, ResourceManager.obtenerAnimacion("Agua"));
        }else if(tipo.equalsIgnoreCase("Lava")){
            image = new Animation(0.2f, ResourceManager.obtenerAnimacion("Lava"));
        }
        frame = (TextureRegion) image.getKeyFrame(0);
        rect = new Rectangle(x, y, frame.getRegionWidth(), frame.getRegionHeight());
    }

    /**
     * Metodo para ejecutar la animacion
     * @param dt delta time
     */
    public void update(float dt){
        tiempo+=dt;
        frame = (TextureRegion) image.getKeyFrame(tiempo, true);
    }

    /**
     * Dibuja el agua
     * @param batch magia para dibujar el agua
     */
    public void render (Batch batch){
        batch.draw(frame, rect.x, rect.y);
    }
}
